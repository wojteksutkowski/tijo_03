package com.zad2;

public class MaterialPoint2D {
    public double x;
    public double y;
    public double mass;

    public MaterialPoint2D(double x, double y, double mass) {
        this.x = x;
        this.y = y;
        this.mass = mass;
    }

    public String toString() {
        return "x = " + x + ", y = " + y + ", mass = " + mass;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getMass() {
        return mass;
    }
}
