package com.zad1;

import java.util.List;

public interface CsvGenerator {
    void toCsv(List<String> list);
}
