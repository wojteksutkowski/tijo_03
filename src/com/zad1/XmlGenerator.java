package com.zad1;

import java.util.List;

public interface XmlGenerator {
    void toXml(List<String> list);
}
