package com.zad1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGeneratorWriter implements CsvGenerator {
    @Override
    public void toCsv(List<String> list)
    {
        try {
            FileWriter csvWriter = new FileWriter("new.csv");
            // pretty line
            csvWriter.append("*************");
            csvWriter.append("\n");
            // new line + '\n'

            for (String line : list) {
                csvWriter.append(line);
            }

            csvWriter.append("\n");
            // pretty line
            csvWriter.append("*************");
            csvWriter.append("\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
