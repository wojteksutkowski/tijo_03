package com.zad1;

import java.util.List;

public interface PdfGenerator {
    void toPdf(List<String> list);
}
